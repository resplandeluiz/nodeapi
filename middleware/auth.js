const jwt = require('jsonwebtoken');
const env = require('../enviroments/env');

const auth = (req, res, next) => {
    const token_header = req.headers.auth;
    if (!token_header) return res.status(401).send({ error: "Usuário sem token!" })
    jwt.verify(token_header, env.JWT_PASS, (err, decode) => {
        if (err) return res.status(401).send({ error: 'Token inválido!' });
        res.locals.auth_data = decode;
        return next();
    });
}

module.exports = auth;