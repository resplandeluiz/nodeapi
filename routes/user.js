const express = require('express');
const router = express.Router();
const Users = require('../models/user');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const env = require('../enviroments/env');


const createUserToken = (userId) => { return jwt.sign({ id: userId }, env.JWT_PASS, { expiresIn: env.JWT_EXPIRES }); };

router.get('/', async (req, res) => {
    try {
        const users = await Users.find({});
        return res.send(users);
    } catch (err) {
        return res.status(500).send({ error: 'Erro na consulta de usuários!' })
    }
});


router.post('/create', async (req, res) => {
    const { email, password } = req.body;
    if (!email || !password) return res.status(400).send({ error: 'Envie os dados corretamente !' });
    try {
        if (await Users.findOne({ email })) return res.status(400).send({ error: 'Usuário já registrado!' });
        const user = await Users.create(req.body);
        user.password = undefined;
        return res.status(201).send({ user, token: createUserToken(user.id) });
    } catch (err) {
        return res.status(500).send({ error: 'Erro ao inserir o usuário' });
    }
});



router.post('/auth', async (req, res) => {
    const { email, password } = req.body;
    if (!email || !password) return res.status(400).send({ error: 'Envie os dados corretamente !' });
    try {
        const user = await Users.findOne({ email }).select('+password');
        if (!user) res.send({ error: 'Usuário não registrado' });
        const password_ok = await bcrypt.compare(password, user.password);
        if (!password_ok) res.status(401).send({ error: "Erro na autenticação do usuário" });
        user.password = undefined;
        return res.status(201).send({ user, token: createUserToken(user.id) });
    } catch (err) {
        return res.status(500).send({ error: 'Erro ao buscar o usuário' });
    }
});


module.exports = router;

/*

    200 - Ok -
    201 - Created -
    202 - Accepted - Processamento não imediato, processando.

    400 - Bad Request - Deu ruim.
    401 - Unauthorized - Authenticação, caráter temporário.
    403 - Forbidden - Authorization, tem caráter permanente
    404 - Not Found -

    500 - Internal server error -
    501 - Not implemented - Api não tem esta funcionalidade
    503 - Service Unavailable - Api executa mas está indisponível


*/