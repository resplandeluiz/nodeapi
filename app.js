const express = require('express');
const app = express();
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const env = require('./enviroments/env');

const url = env.DB_CONNECTION;
const options = { reconnectTries: Number.MAX_VALUE, reconnectInterval: 500, poolSize: 5, userNewUrlParser: true };

mongoose.connect(url, options);
mongoose.set('userCreateIndex', true);

mongoose.connection.on('error', (err) => {
    console.log('Erro na conexão com o MongoDB' + err);
});
mongoose.connection.on('disconnected', (err) => {
    console.log('Aplicação disconectada com o banco de dados' + err);
});

mongoose.connection.on('connected', () => {
    console.log('applicattion connected to mongoDb');
});

// Body Parser
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

const indexRouter = require('./routes/index');
const userRouter = require('./routes/user');

app.use('/',indexRouter);
app.use('/users',userRouter);

app.listen(3000);
module.exports = app;